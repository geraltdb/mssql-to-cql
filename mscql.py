import pymssql
from dotenv import dotenv_values
import logging as log
import json
import pandas as pd
from sqlalchemy import create_engine
from sqlalchemy.engine import URL
from cassandra.cluster import Cluster


env = dotenv_values()
log.basicConfig(level=log.DEBUG, format='%(asctime)s - %(levelname)s - %(message)s')

class MsSQL:
    def __init__(self) -> None:
        self.connect()

    def sql_to_cql_type(self,sql_type_name:str)->str:
        sql_type_name = sql_type_name.upper()
        if sql_type_name in ['CHAR', 'NCHAR', 'VARCHAR', 'NVARCHAR',]:
            return "text"

        if sql_type_name == 'UNIQUEIDENTIFIER':
            return "text"

        if sql_type_name == 'XML':
            return "text"

        if sql_type_name in ['BINARY', 'IMAGE', 'VARBINARY']:
            return "blob"
            
        if sql_type_name in ['ROWVERSION', 'TIMESTAMP']:
            return "timestamp"
        
        if sql_type_name == 'BIT':
            return "tinyint"

        if sql_type_name == 'TINYINT':
            return "tinyint"

        if sql_type_name == 'SMALLINT':
            return "smallint"

        if sql_type_name in ('INTEGER','INT','MEDIUMINT'):
            return "int"

        if sql_type_name == 'BIGINT':
            return "bigint"

        if sql_type_name in ("NUMERIC", "DECIMAL"):
            return "decimal"

        if sql_type_name == "DOUBLE":
            return "decimal"

        if sql_type_name == "SMALLMONEY":
            return "double"
        
        if sql_type_name == "MONEY":
            return "double"

        if sql_type_name == "FLOAT":
            return "float"

        if sql_type_name == "REAL":
            return "double"
        
        if sql_type_name == "DATE":
            return "date"

        if sql_type_name == "TIME":
            return "time"
        
        if sql_type_name == "DATETIME":
            return "timestamp"
        
        if sql_type_name == "TIMESTAMP":
            return "timestamp"
        
        if sql_type_name == "YEAR":
            return "int"
        
        if sql_type_name in ["BOOL","BOOLEAN"]:
            return "boolean"

        return "text"
    
    def connect(self) -> None:
        # get data from env
        self.SERVER = env.get("MSSQL_DB_SERVER")
        self.USER = env.get("MSSQL_DB_USERNAME")
        self.PASSWORD = env.get("MSSQL_DB_PASSWORD")
        self.DATABASE = env.get("MSSQL_DB_NAME")
        self.PORT = env.get("MSSQL_DB_PORT")
        self.INCLUDE_SCHEMAS = [i for i in env.get("INCLUDE_SCHEMA").split(',') if i!=""]
        try:
            log.debug("Database connecting...")
            self.conn = pymssql.connect(self.SERVER,self.USER,self.PASSWORD,self.DATABASE)
            self.cursor = self.conn.cursor()
            log.debug("Database connected...")
            f = open("schema.cache","w+")
            f.close()
            # start setup
            self.setup();
        except Exception as e:
            print(e)
            log.error("Database not connect..")
    
    def setup(self):
        # get tables list
        self.schemas = ",".join(["'"+i+"'" for i in self.INCLUDE_SCHEMAS])
        self.cursor.execute("SELECT CONCAT(T.TABLE_SCHEMA,'.',T.TABLE_NAME) tbl FROM INFORMATION_SCHEMA.TABLES T WHERE T.TABLE_TYPE='BASE TABLE' AND T.TABLE_SCHEMA IN (%s)"%(self.schemas))
        self.schema_tables = list(map(lambda x:x[0],self.cursor.fetchall()))
        log.debug("Tables loaded...")
        # get tables columns and data type
        input_tables = ",".join(["'"+i+"'" for i in self.schema_tables])
        self.cursor.execute("SELECT CONCAT(C.TABLE_SCHEMA,'.',C.TABLE_NAME),C.COLUMN_NAME,C.DATA_TYPE FROM INFORMATION_SCHEMA.COLUMNS C WHERE CONCAT(C.TABLE_SCHEMA,'.',C.TABLE_NAME) IN (%s)"%(input_tables))
        self.schema_tables_data = self.cursor.fetchall()
        log.debug("Table data loaded...")
        # get table and column which have primary keys
        self.cursor.execute("SELECT CONCAT(T.TABLE_SCHEMA,'.',T.TABLE_NAME) TABLENAME,C.COLUMN_NAME  CONSTRAINTNAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS T JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE C ON C.CONSTRAINT_NAME=T.CONSTRAINT_NAME WHERE CONCAT(T.TABLE_SCHEMA,'.',T.TABLE_NAME) IN(%s)"%(input_tables))
        self.schema_tables_constraints = self.cursor.fetchall()
        log.debug("Table constraints loaded...")
        # get tables with pk
        self.schema_tables = set(map(lambda x: x[0],self.schema_tables_constraints))
        # verify and generate schema
        self.generate_schema()
        #self.generate_csv()

    # verify and generate schema.json
    # this schema is to generate cql tables
    def generate_schema(self):
        # read cache
        f = open("schema.cache","r")
        lns = f.readlines()
        f.close()
        is_new = False
        if len(lns)==0:
            is_new=True
            
        if is_new==False:
            cache = [ l.replace("\n","").split(",") for l in lns if l!=""]
            cache = { k:int(v) for k,v in cache}
            new_cache={}
            for table in self.schema_tables:
                count=0
                for table_cols in self.schema_tables_data:
                    if table==table_cols[0]:
                        count+=1
                new_cache[table]=count
            
            if len(cache.keys())==len(new_cache.keys()):
                flag=True
                o = sorted(list(cache.keys()))
                n = sorted(list(new_cache.keys()))
                if o==n:
                    for k in new_cache.keys():
                        if cache[k]==new_cache[k]:
                            flag=False
                if flag:
                    is_new=True
                    log.debug("Changes detected in schema...")
                else:
                    log.debug("No changes detected in schema...")
            else:
                is_new=True

        if is_new:
            log.debug("Schema generation started...")
            f = open("schema.cache","w")
            for table in self.schema_tables:
                count=0
                for table_cols in self.schema_tables_data: 
                    if table==table_cols[0]:
                        count+=1
                f.write(table+","+str(count)+"\n")
            f.close()

            self.schema_doc = {}
            for table in self.schema_tables:
                tmp_dict={}
                for table_cols in self.schema_tables_data:
                    if table==table_cols[0]:
                        #{ column_name: datatype}
                        tmp_dict[table_cols[1]]=self.sql_to_cql_type(table_cols[2])
                tmp_pks=[col[1] for col in self.schema_tables_constraints if col[0]==table]
                self.schema_doc[table.replace(".","_")]={
                    "columns":tmp_dict.copy(),
                    "pk":tmp_pks.copy()
                }
                tmp_pks.clear()
                tmp_dict.clear()
            f = open("schema.json","w")
            f.write(json.dumps(self.schema_doc))
            f.close()
            log.debug("Schema generated...")

    def generate_csv(self):
        try:
            connection_string = "DRIVER={ODBC Driver 18 for SQL Server};SERVER="+self.SERVER+";DATABASE="+self.DATABASE+";UID="+self.USER+";PWD="+self.PASSWORD+";TrustServerCertificate=yes;"
            connection_url = URL.create("mssql+pyodbc", query={"odbc_connect": connection_string})
            engine = create_engine(connection_url)
        except Exception as e:
            raise e

        for schema_table in self.schema_tables:
            schema,table = schema_table.split(".")
            try:
                df = pd.read_sql_table(table_name=table,con=engine,schema=schema)
                df.to_csv(".temp/"+schema_table.replace(".","_")+".csv",index=None)
            except Exception as e:
                print(e)
                log.error("CSV not generated for >> "+schema_table)
            log.debug("CSV generated for >> "+schema_table)
        engine.clear_compiled_cache()
        engine.dispose()

class Scylla:
    def __init__(self) -> None:
        self.KEYSPACE = env.get("SCYLLA_KEYSPACE")
        self.HOST = env.get("SCYLLA_HOST")
        self.PORT = env.get("SCYLLA_PORT")
        self.cluster = Cluster([self.HOST],port=self.PORT)
        self.session = self.cluster.connect(self.KEYSPACE)
        fp = open("schema.json")
        self.schema_tables = json.load(fp)
        fp.close()

    def generate_tables(self):
       
        self.str_tables = []
        fp = open("cql_schema.sql","w")
        for table,data in self.schema_tables.items():
            cql = "CREATE TABLE %s \n(\n"%(table)
            l = []
            for column,dtype in data["columns"].items():
                l.append("\t%s %s"%(column,dtype))
            cql+=",\n".join(l.copy())
            pk = list(set(data["pk"]))
            if len(pk)>1:
                cql+=",\n\tPRIMARY KEY((%s))\n);\n"%(",".join(pk))
            else:
                cql+=",\n\tPRIMARY KEY(%s)\n);\n"%(pk[0])
            
            self.str_tables.append(cql)
            dcp = "DROP TABLE IF EXISTS %s; \n"%(table)+cql
            fp.write(dcp)
            l.clear()
        fp.close()

    # create tables in db
    def create_tables(self):
        self.generate_tables()
        tb = list(self.schema_tables.keys())
        idx = 0
        for cql in self.str_tables:
            try:
                self.session.execute(cql)
                log.debug("Table generated... >> %s"%(tb[idx]))
            except Exception as e:
                log.error(e)
                log.error("Table not generated... >> %s"%(tb[idx]))
            idx+=1
    # set records in db
    def set_records(self):
        self.create_tables() # create tables
        mssqlObj = MsSQL() # object os MsSQL class
        mssqlObj.generate_csv() # generate csv for each tables
        for table,data in self.schema_tables.items():
            cols = data["columns"].keys() # table columms
            param_keys = ",".join(cols) # e.g id,name
            param_values = ",".join([ '%('+k+')s' for k in cols])
            statement = "INSERT INTO "+table+" ("+param_keys+") VALUES ("+param_values+")"
            df = pd.read_csv(".temp/%s.csv"%(table),index_col=None)
            c = 0
            for row in df.to_dict(orient="records"):
                self.session.execute(statement,row)
                c+=1
            log.debug("Added %s records in %s "%(str(c),table))
            

