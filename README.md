# MSSQL_TO_CQL

### Required ODBC 18 Driver:
install below mention driver:
``` 
https://learn.microsoft.com/en-us/sql/connect/odbc/linux-mac/installing-the-microsoft-odbc-driver-for-sql-server?view=sql-server-ver16&tabs=alpine18-install%2Calpine17-install%2Cdebian8-install%2Credhat7-13-install%2Crhel7-offline
```

### install python packages
``` bash
pip install pymssql==2.2.11 SQLAlchemy==2.0.25 pandas==2.2.0
```

### If you change database then delete all file into .temp & all .cache files